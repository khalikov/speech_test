FROM python:3.7

RUN apt update && apt install -y default-libmysqlclient-dev 

RUN pip install flask celery yadisk cryptography mysqlclient

ADD src /opt
WORKDIR /opt

CMD ["python3", "app.py"]

