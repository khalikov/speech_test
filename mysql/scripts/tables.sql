ALTER USER 'speech'@'%' IDENTIFIED WITH mysql_native_password BY 'speech3214';

USE speech;

CREATE TABLE IF NOT EXISTS users_info (
	user_id INT,
	last_modified_date TIMESTAMP,
	PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS calls (
	user_id INT,
	id VARCHAR(100),
	filename VARCHAR(256),
	date TIMESTAMP,
	status ENUM('ACCEPTED', 'REJECTED'),
	type ENUM('INCOMING', 'OUTGOING', 'LOCAL'),
	phone_number_operator VARCHAR(20),
	phone_number_client VARCHAR(20),
	duration_answer INT(2),
	PRIMARY KEY (id),
	UNIQUE(filename, user_id)
);

