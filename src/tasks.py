from app import app, execute
import uuid
import datetime
import yadisk
import logging
import csv
import os


@app.task
def check_all_calls():
    """
    Celery task for getting all calls for tokens
    """
    for token in [
        os.environ.get('DEFAULT_TOKEN')
        # other users
    ]:
        check_calls_csv.delay(token)


@app.task
def check_calls_csv(token):
    """
    Celery task for checking calls for token
    """
    y = yadisk.YaDisk(token=token)
    disk_info = y.get_disk_info()
    user_id = disk_info.user.uid
    meta = y.get_meta('meta/calls-info.csv')
    sql0 = """
SELECT last_modified_date FROM users_info
WHERE user_id = %s
LIMIT 1
    """
    res = execute(sql0, user_id)
    last_modified_date = None
    if res and res[0]:
        last_modified_date = res[0][0]
    if meta.modified == last_modified_date:
        logging.debug('not modified')
        return
    sql1 = """
REPLACE INTO users_info (user_id, last_modified_date)
VALUES (%s, %s)
    """
    logging.debug(sql1)
    execute(sql1, user_id, meta.modified.strftime('%Y-%m-%dT%H:%M:%S'))
    result = []
    y.download('/meta/calls-info.csv', '/meta/calls-info-%s.csv' % user_id)
    with open('/meta/calls-info-%s.csv' % user_id) as csvfile:
        reader = csv.reader(csvfile)
        for i, row in enumerate(reader):
            row2 = [x for x in row]
            # if it's a header, add `user_id` header column
            if not i:
                row2.append('user_id')
                row2.append('id')
            else:

                # add user_id attribute
                row2.append(user_id)
                row2.append(uuid.uuid4().hex)
            result.append(row2)
            if len(result[0]) != len(row2):
                raise Exception('Not enough data')
            # other tests including test for sql injection
    sql = """
        INSERT INTO calls ({}) VALUES {}
        ON DUPLICATE KEY UPDATE user_id = %s
    """.format(
        ', '.join([x for x in result[0]]),
        '(' + '), ('.join([
            ', '.join([f"'{x}'" for x in row]) for row in result[1:]
        ]) + ')'
    )
    logging.debug(sql)
    execute(sql, user_id)
