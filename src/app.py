from yadisk import YaDisk
import csv
import MySQLdb
import os
import json

from celery import Celery
from flask import Flask, request, Response, send_file
from celery.schedules import crontab

flask_app = Flask(__name__)
osenv = os.environ.get


def get_token():
    """
    Getting token from headers or ENV variable
    """
    headers = request.headers
    token = headers.get('Authorization', 'Bearer none').split(' ')[1]
    if token == 'none':
        return osenv('DEFAULT_TOKEN')
    return token


# BROKER SETTINGS
scheme = osenv('CELERY_BROKER_SCHEME')
user = osenv('CELERY_BROKER_USER')
password = osenv("CELERY_BROKER_PASSWORD")
host = osenv("CELERY_BROKER_HOST")
port = osenv("CELERY_BROKER_PORT")

# CELERY SETTINGS
app = Celery(
    broker=f"{scheme}://{user}:{password}@{host}:{port}",
    include=['tasks']
)

app.conf.beat_schedule = {
    'check_all_calls': {
        'task': 'tasks.check_all_calls',
        'schedule': 60.0
    }
}


def execute(sql, *args):
    """
    All SQL must be processed here
    """
    db = None
    cursor = None
    try:
        db = MySQLdb.connect(os.environ.get("MYSQL_HOST", "speech_mysql"),
                             os.environ.get("MYSQL_USER", "speech"),
                             os.environ.get("MYSQL_PASSWORD", "speech3214"),
                             os.environ.get("MYSQL_DB", "speech"),
                             autocommit=True)
        cursor = db.cursor()
        cursor.execute(sql, args)
        if sql.strip().lower().startswith('select'):
            return [x for x in cursor.fetchall()]
    finally:
        if cursor:
            cursor.close()
        if db:
            db.close()


@flask_app.route("/calls")
def get_calls():
    """
    /calls method. We are checking for
    - date_from
    - date_till
    - limit
    - offset
    query params and return result based on their presence
    """
    y = YaDisk(token=get_token())
    disk_info = y.get_disk_info()
    user_id = disk_info.user.uid

    date_from = request.args.get('date_from')
    if not date_from or not date_from.isdigit() or len(date_from) > 20:
        return 'date_from is not properly filled', 400
    date_till = request.args.get('date_till')
    if not date_till or not date_till.isdigit() or len(date_till) > 20:
        return 'date_till is not properly filled', 400
    limit = request.args.get('limit', '100')
    offset = request.args.get('offset', '0')
    if limit.isdigit() and len(limit) < 4:
        limit = int(limit)
    else:
        limit = 100
    if offset.isdigit() and len(offset) < 10:
        offset = int(offset)
    else:
        offset = 0
    sql = """
SELECT id,
       type,
       date,
       duration_answer,
       status,
       phone_number_client,
       phone_number_operator
FROM calls
WHERE user_id = %s
LIMIT %s
OFFSET %s
    """
    res = execute(sql, user_id, limit, offset)
    return json.dumps({'calls': [
        {
            'id': x[0],
            'type': x[1],
            'date': x[2].timestamp(),
            'duration_answer': x[3],
            'status': x[4],
            'phone_number_client': x[5],
            'phone_number_operator': x[6]
        } for x in res
    ]})


@flask_app.route("/recording")
def get_recording():
    """
    We are downloading recording by its
    - `id` -> get filename -> get from yadisk of user_id
    and
    """
    if not os.path.exists('/connect'):
        os.makedirs('/connect')
    y = YaDisk(token=get_token())
    disk_info = y.get_disk_info()
    user_id = disk_info.user.uid

    call_id = request.args.get('call_id')
    sql = """
SELECT filename
FROM calls
WHERE id = %s
LIMIT 1
    """
    res = execute(sql, call_id, user_id)
    if res and res[0]:
        filename = res[0][0]
    if filename.endswith('.wav'):
        mime = 'audio/wav'
    elif filename.endswith('.mp3'):
        mime = 'audio/mpeg'
    elif filename.endswith('.ogg'):
        mime = 'audio/ogg'
    else:
        return 'unknown type of file', 400

    local_path = '/connect/%s.%s' % (call_id, filename.split('.')[-1])
    y.download('/speechanalytics-connect/%s' % filename, local_path)
    try:
        return send_file(local_path, as_attachment=True)
    except FileNotFound:
        abort(404)


@flask_app.route('/operators')
def get_operators():
    """
    Getting operators of user
    """
    y = YaDisk(token=get_token())
    disk_info = y.get_disk_info()
    user_id = disk_info.user.uid

    sql = """
SELECT phone_number_operator
FROM calls
GROUP BY phone_number_operator, user_id
HAVING user_id = %s
    """
    res = execute(sql, user_id)
    operators = []
    for result in res:
        operators.append({
            'name': result[0],
            'phone_number': result[0]
        })
    return json.dumps({
        'operators': operators
    })
